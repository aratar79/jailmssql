import sqlite3
import configparser
import os

from sqlite3 import Error


class Connection:

    def __init__(self):

        try:

            self.config = configparser.ConfigParser()
            self.config.read('config.ini')
            self.exists = False

            self.path = self.config['DEFAULT']['PATH']
            self.filename = self.config['DEFAULT']['FILENAME']

            if os.path.isfile(self.path + '/' + self.filename):
                self.exists = True
            else:
                con = sqlite3.connect(self.path + '/banips.db')
                cur = con.cursor()
                cur.execute("CREATE TABLE ips (Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, Ip	TEXT NOT NULL, Pais	TEXT)")
                con.commit()
                con.close()

        except Error as e:

            print(e)

    def insert(self, ip, country):

        try:

            con = sqlite3.connect(self.path + '/banips.db')
            cur = con.cursor()
            entity = (ip, country)
            cur.execute('INSERT INTO Ips(Ip, Pais) VALUES(?, ?)', entity)
            con.commit()
            con.close()
        
        except Error as e:

            print(e)

    def get_all(self):

        try:

            con = sqlite3.connect(self.path + '/banips.db')
            cur = con.cursor()
            cur.execute("SELECT * FROM Ips")
            rows = cur.fetchall()
            return rows

        except:

            pass
    
    def exists_ip(self, ip):

        try:

            con = sqlite3.connect(self.path + '/banips.db')
            cur = con.cursor()
            cur.execute("SELECT True FROM Ips WHERE Ip = ?", [ip])
            result = cur.fetchall()
            if result:
                self.exists = True
                return self.exists
            else:
                self.exists = False
                return self.exists
        
        except Error as e:

            print(e)

